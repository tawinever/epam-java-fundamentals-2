package util;

import java.util.HashSet;

public class StringHelper {

    public static int getNumberOfUniqueDigits(String number) {
        HashSet<Character> digitPool =new HashSet<>();
        for (int i = 0; i < number.length(); ++i) {
            digitPool.add(number.charAt(i));
        }
        int badCharacters = 0;
        if (digitPool.contains('-')) {
            badCharacters++;
        }

        if (digitPool.contains('.')) {
            badCharacters++;
        }

        return digitPool.size() - badCharacters;
    }

    public static boolean isDigitsOfNumberStrictlyIncreasing(String number) {
        int curDigit = -1;
        for (int i = 0; i < number.length(); ++i) {
            if (number.charAt(i) == '-' || number.charAt(i) == '.') {
                continue;
            }

            if (curDigit < number.charAt(i) - '0') {
                curDigit = number.charAt(i) - '0';
            } else {
                return false;
            }
        }
        return true;
    }

    public static boolean isDigitsOFNumberUnique(String number) {
        HashSet<Character> digitPool = new HashSet<>();
        for (int i = 0; i < number.length(); ++i) {
            if (digitPool.contains(number.charAt(i))) {
                return false;
            } else {
                digitPool.add(number.charAt(i));
            }
        }
        return true;
    }
}
