import util.StringHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static String PROGRAM_FAILED = "Bad Input Data, try another numbers";
    public static void main(String[] args) {
        System.out.println("Hello, you need enter all numbers in one line(divided by whitespace)");
        System.out.println("Enter numbers:");

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String[] nums = null;
        try {
            nums = bufferedReader.readLine().trim().split("\\s+");
        } catch (Exception e) {
            System.out.println(PROGRAM_FAILED);
            e.printStackTrace();
            return;
        }

        try {
            bufferedReader.close();
        } catch (IOException e) {
            System.err.println("Problems with closing buffered reader" + e.getMessage());
        }

        if (nums.length == 0) {
            System.out.println(PROGRAM_FAILED);
            return;
        }

        String[] shortestAndLongest = findShortestAndLongestNumber(nums);

        System.out.println(String.format("1. The shortest number: %s(%d chars), the longest number: %s(%d chars)",
                shortestAndLongest[0],
                shortestAndLongest[0].length(),
                shortestAndLongest[1],
                shortestAndLongest[1].length()));

        System.out.println(String.format("2. Number with the least unique digit(s) is: %s",
                findFewestUniqueDigitNumber(nums)));

        System.out.println(String.format("3. Number with strictly increasing digits is: %s",
                findNumberWithStrictlyIncreasingDigits(nums)));

        System.out.println(String.format("4. Number with unique digits is: %s", findNumberWithUniqueDigits(nums)));
    }

    public static String[] findShortestAndLongestNumber(String[] nums) {
        int minLength = Integer.MAX_VALUE;
        int maxLength = 0;
        String shortest = null;
        String longest = null;

        for (var num : nums) {
            if (num.length() < minLength) {
                minLength = num.length();
                shortest = num;
            }

            if (num.length() > maxLength) {
                maxLength = num.length();
                longest = num;
            }
        }

        return new String[]{shortest, longest};
    }

    public static String findFewestUniqueDigitNumber(String[] nums) {
        int numberOfUniqueDigits = Integer.MAX_VALUE;
        String numberWithLeastUniqueDigits = null;
        for (var num : nums) {
            int curNumberOfUniqueDigits = StringHelper.getNumberOfUniqueDigits(num);
            if (curNumberOfUniqueDigits < numberOfUniqueDigits) {
                numberOfUniqueDigits = curNumberOfUniqueDigits;
                numberWithLeastUniqueDigits = num;
            }
        }
        return numberWithLeastUniqueDigits;
    }

    public static String findNumberWithStrictlyIncreasingDigits(String[] nums) {
        for (var num : nums) {
            if (StringHelper.isDigitsOfNumberStrictlyIncreasing(num))
                return num;
        }
        return null;
    }

    public static String findNumberWithUniqueDigits(String[] nums) {
        for (var num : nums) {
            if (StringHelper.isDigitsOFNumberUnique(num)) {
                return num;
            }
        }
        return null;
    }
}
